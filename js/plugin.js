$(function(){

    var controlElements = {
        'undo': document.getElementById('undoBtn'),
        'redo': document.getElementById('redoBtn'),
        'colorPicker': document.getElementById('colorPicker'),
        'rangeSlider': document.getElementById('sizeSlider'),
        'canvasClear': document.getElementById('canvasClear'),
        'productRemove': document.getElementById('productRemove')
    };
    var mainSections = {
        'productList': document.getElementById('productsList'),
        'selectedProductsList': document.getElementById('selectedProductsList'),
        'canvas': document.getElementById('productFrame')
    };
    var activeProduct;
    var products = new ProductsList();
    var selectedProductsList = new SelectedProductsList();
    var canvasFrame = new CanvasFrame();
    var preview_image_url = 'images/sample-photo.svg';
    var counter = 0;

    products.init();
    canvasFrame.initColorPicker();
    canvasFrame.initRangeSlider();

    function ProductsList(){
        var _this = this;
        this.list = getProducts();
        this.types = getProductTypes();

        this.init = function(){
            $.each(this.types, function(key, value){
                var titleTemplate = '<label class="title">' + value.type_name + '</label>';
                mainSections.productList.innerHTML += titleTemplate;
                _this.createList(value.id);
            });
            this.addButtonsEvents();
        };
        this.createList = function(type_id){
            var listElem = document.createElement('ul');
            $.each(this.list, function(key, value){
                var itemTemplate = '<li><a class="add-products-btn" href="#" data-product-id="' + key + '"><span>' + value.product + '</span><span class="icon icon-plus"></span></a></li>';
                if(value.product_type == type_id) listElem.innerHTML += itemTemplate;
            });
            mainSections.productList.append(listElem);
        };
        this.addButtonsEvents = function(){
            var buttons = $(mainSections.productList).find('a');
            $.each(buttons, function(e, elem){
                console.log(products.selectProduct);
                elem.addEventListener('click', products.selectProduct, false);
            });
        };
        this.selectProduct = function(e){
            e.preventDefault();
            selectedProductsList.addSelectedProduct(e.currentTarget.dataset.productId);
        };
        this.getProductById = function(id){
            return this.list[id];
        };

        function getProducts(){
            var result = [{
                "id": "1",
                "product": "Mandelic Acid",
                "product_type": "1"
            }, {
                "id": "2",
                "product": "Botox 6ml",
                "product_type": "2"
            }, {
                "id": "3",
                "product": "Restylane 3ml",
                "product_type": "2"
            }, {
                "id": "4",
                "product": "Salcylic Acid",
                "product_type": "1"
            }, {
                "id": "5",
                "product": "Botox 2ml",
                "product_type": "2"
            }, {
                "id": "6",
                "product": "Restylane 9ml",
                "product_type": "2"
            }];
            return result || {};
        }
        function getProductTypes(){
            var result = [{
                "id": 1,
                "type_name": "Acid"
            },{
                "id": 2,
                "type_name": "Injections"
            }];
            return result || {};
        }
    }
    function SelectedProductsList(){
        var _this = this;
        this.list = {};
        this.selected = 0;

        this.addButtonsEvents = function(){
            var buttons = $(mainSections.selectedProductsList).find('li');
            $.each(buttons, function(e, elem){
                elem.children[0].addEventListener('click', _this.setActive, false);
            });
        };
        this.addSelectedProduct = function(id){
            this.list[counter] = products.getProductById(id);
            this.selectProduct();
            counter++;
            canvasFrame.init();
            this.initRemoveBtn();
        };
        this.selectProduct = function(){
            _this.selected = counter;
            _this.getSelectedProducts(_this.selected);
            _this.addButtonsEvents();
        };
        this.setActive = function(e){
            var before_id = _this.selected;
            if(typeof e !== "undefined"){
                e.preventDefault();
                _this.selected = e.target.dataset.itemId;
                _this.saveCanvasState(before_id);
            }else{
                _this.selected = 0;
            }
            console.log(_this.selected);
            _this.loadCanvasState(_this.selected);
            _this.getSelectedProducts(_this.selected);
            _this.addButtonsEvents();
            _this.initRemoveBtn(_this.selected);
        };
        this.getSelectedProducts = function(id){
            var activeId = id;
            mainSections.selectedProductsList.innerHTML = "";

            $.each(this.list, function(key, value){
                var data = products.getProductById(value.id);

                console.log(data);
                var isActive = (activeId == key) ? 'active' : '';
                mainSections.selectedProductsList.innerHTML += '<li class="' + isActive + '"><a class="select-product-btn" href="#" data-item-id="'+ key +'" data-product-id="'+ value.id +'" data-product-type="'+ this.product_type +'">'+ value.product +'</a></li>'
            });
        };
        this.initRemoveBtn = function(){
            console.log(_this.list);

            controlElements.productRemove.dataset.originalTitle = 'Remove ' + _this.list[_this.selected].product;
            controlElements.productRemove.addEventListener('click', this.removeProduct, false);
        };
        this.removeProduct = function(){
            delete _this.list[_this.selected];
            _this.getSelectedProducts();
            _this.setActive();
        };
        this.saveCanvasState = function(before_id){
            if(typeof this.list[before_id] !== 'object') return false;
            _this.list[before_id].historyState = canvasFrame.canvas.toDataURL('image/png') || '';
        };
        this.loadCanvasState = function(id){
            var img = new Image;
            img.onload = function(){
                canvasFrame.ctx.drawImage(img, 0, 0);
            };
            img.src = (_this.list[id].historyState !== 'undefined') ? _this.list[id].historyState : preview_image_url;
        };
    }
    function CanvasFrame(){
        var _this = this;
        this.canvas = mainSections.canvas;
        this.ctx = this.canvas.getContext('2d');
        this.isDrawing = false;
        this.history = new CanvasHistory(this.canvas, this.ctx);

        this.init = function(){
            this.loadBackground();
            this.addCanvasEvents();
            this.brushSizeEvents();
            this.colorPickerEvents();
            this.lineWidth = this.rangeSlider.data('slider').getValue();
            this.lineColor = this.colorPicker.colorpicker('getValue');
        };
        this.startDraw = function(e){
            var x = e.pageX - $(_this.canvas).offset().left;
            var y = e.pageY - $(_this.canvas).offset().top;
            _this.ctx.beginPath();
            artisan.drawCircle($(_this.canvas).prop('id'), x, y, _this.lineWidth, _this.lineColor);
            //_this.history.saveState(_this.canvas);
            this.isDrawing = true;
        };
        this.stopDraw = function(){
            if(this.isDrawing) this.isDrawing = false;
        };
        this.draw = function(e){
            if(!this.isDrawing) return false;
            var x = e.pageX - $(_this.canvas).offset().left;
            var y = e.pageY - $(_this.canvas).offset().top;
            artisan.drawCircle($(_this.canvas).prop('id'), x, y, _this.lineWidth, _this.lineColor);
        };
        this.brushSizeEvents = function(){
            this.rangeSlider.on('slide', function(e){
                _this.lineWidth = e.value;
            });
        };
        this.colorPickerEvents = function(){
            this.colorPicker.on('changeColor', function(e){
                _this.lineColor = e.color.toHex();
            });
        };
        this.canvasClear = function(e){
            _this.ctx.clearRect(0, 0, _this.canvas.width, _this.canvas.height);
            _this.loadBackground();
        };
        this.loadBackground = function(){
            _this.ctx.strokeStyle = "red";
            _this.ctx.rect(0, 0, _this.canvas.width, _this.canvas.height);
            var img = new Image();
            img.onload = function(){
                _this.ctx.drawImage(img, 0, 0, _this.canvas.width, _this.canvas.height);
            };
            img.src = preview_image_url;
        };
        this.initColorPicker = function(){
            var pickerOptions = {
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            };
            this.colorPicker = $(controlElements.colorPicker).colorpicker(pickerOptions);
        };
        this.initRangeSlider = function(){
            var rangeSliderOptions = {
                orientation: 'vertical',
                tooltip_position:'left'
            };
            this.rangeSlider = $(controlElements.rangeSlider).slider(rangeSliderOptions);
        };
        this.addCanvasEvents = function(){
            this.canvas.addEventListener('mousedown', this.startDraw, false);
            this.canvas.addEventListener('mouseup', this.stopDraw, false);
            this.canvas.addEventListener('mousemove', this.draw, false);
            this.canvas.addEventListener('mouseout', this.stop, false);
            controlElements.canvasClear.addEventListener('click', this.canvasClear, false);
            //controlElements.undo.addEventListener('click', _this.history.undo.bind(_this.canvas, _this.ctx), false);
            //controlElements.redo.addEventListener('click', _this.history.redo.bind(_this.canvas, _this.ctx), false);
        };
    }
    function CanvasHistory(canvas, ctx){
        var _this = this;
        this.redo_list = {};
        this.undo_list = {};

        this.saveState = function(canvas, list, keep_redo) {
            keep_redo = keep_redo || false;
            if(!keep_redo) {
                _this.redo_list = [];
            }

            _this.undo_list += canvas.toDataURL();
        };
        this.undo = function(canvas, ctx) {
            _this.restoreState(canvas, ctx, _this.undo_list, _this.redo_list);
        };
        this.redo = function(canvas, ctx) {
            _this.restoreState(canvas, ctx, _this.redo_list, _this.undo_list);
        };
        this.restoreState = function(canvas, ctx,  pop, push) {
            if(pop.length) {
                _this.saveState(canvas, push, true);
                var restore_state = pop.pop();
                var img = new Element('img', {'src':restore_state});
                img.onload = function() {
                    ctx.clearRect(0, 0, 600, 400);
                    ctx.drawImage(img, 0, 0, 600, 400, 0, 0, 600, 400);
                }
            }
        }
    }
});