$(function(){

    var canvas;
    var ctx;
    var productsList = $('#productsList');
    var products = getProducts();
    var product_types = getProductTypes();
    var selectedProducts = {};
    var counterId = 0;
    var selectedItem = 0;
    var preview_image_url = 'images/sample-photo.svg';
    var removeProductBtn = $('#productRemove');

    console.log(typeof selectedProducts === "object");

    if(typeof products === "object" && products.length != 0) initProductsList();

    $('.add-products-btn').on('click', function(e){
        e.preventDefault();
        selectProduct($(this));
    });
    removeProductBtn.on('click', function(e){
        e.preventDefault();
        console.log(selectedProducts[selectedItem].getData());
        selectedProducts[selectedItem].removeProduct();
    });
    
    // Color picker init
    var pickerOptions = {
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    };
    var colorPicker = $('#colorPicker').colorpicker(pickerOptions);

    // Range slider
    var rangeSliderOptions = {
        orientation: 'vertical',
        tooltip_position:'left'
    };
    var rangeSlider = $('#sizeSlider').slider(rangeSliderOptions);

    function selectProduct(elem){
        var productObj = getProductById(elem[0].dataset.productId);

        selectedProducts[counterId] = new Product(counterId, productObj.id, productObj.product, productObj.product_type);
        selectedProductsList.innerHTML += selectedProducts[counterId].getTemplate();
        setActiveItem($('#selectedProductsList').find('li:last-child'));
        selectedItem = counterId;

        $('.select-product-btn').on('click', function(e){
            e.preventDefault();
            setActiveItem($(this).parent());
        });

        counterId++;
    }
    function setActiveItem(elem){
        console.log(elem);

        var elems = $('#selectedProductsList').find('li');
        elems.removeClass('active');
        elem.addClass('active');

        selectedItem = elem.children()[0].dataset.itemId;
        //setTooltip(removeProductBtn, {'title': selectedProducts[selectedItem].getData().name});

        //selectedProducts[selectedItem].initCanvasObj();

        if(typeof productFrame === 'object') return false;
        var productFrame = new ProductFrame();
        productFrame.init();
    }
    function getProductById(id){
        return products[id];
    }
    function getProductsByTypes(type_id){
        var listElem = document.createElement('ul');
        $.each(products, function(key, value){
            var template = '<li><a class="add-products-btn" href="#" data-product-id="' + key + '"><span>' + value.product + '</span><span class="icon icon-plus"></span></a></li>';
            if(value.product_type == type_id) listElem.innerHTML += template;
        });
        productsList.append(listElem);
    }
    function getProducts(){

        // TODO: get json data from file
        /*var result = $.getJSON(window.location.href + 'assets/products.json', function(response){
         return response.responseJSON;
         });
         console.log(result);*/

        var result = [{
            "id": "1",
            "product": "Mandelic Acid",
            "product_type": "1"
        }, {
            "id": "2",
            "product": "Botox 6ml",
            "product_type": "2"
        }, {
            "id": "3",
            "product": "Restylane 3ml",
            "product_type": "2"
        }, {
            "id": "4",
            "product": "Salcylic Acid",
            "product_type": "1"
        }, {
            "id": "5",
            "product": "Botox 2ml",
            "product_type": "2"
        }, {
            "id": "6",
            "product": "Restylane 9ml",
            "product_type": "2"
        }];

        return result || {};
    }
    function getProductTypes(){

        // TODO: get json data from file
        var result = [{
            "id": 1,
            "type_name": "Acid"
        },{
            "id": 2,
            "type_name": "Injections"
        }];

        return result || {};

    }
    function setTooltip(elem, properties){
        $.each(properties, function(key, value){
            elem.prop(key, value);
        });
        elem.tooltip('title', value);
    }

    function initProductsList(){
        $.each(product_types, function(key, value){
            productsList.append('<label class="title">' + value.type_name + '</label>');
            getProductsByTypes(value.id);
        });
    }

    function Product(item_id, id, name, type){
        this.item_id = item_id;
        this.id = id;
        this.name = name;
        this.type = type;
        this.template = '<li><a class="select-product-btn" href="#" data-item-id="'+ this.item_id +'" data-product-id="'+ this.id +'" data-product-type="'+ this.type +'">'+ this.name +'</a></li>';
        //this.productFrame;
        this.history = {
            redo_list: [],
            undo_list: [],
            saveState: function(c, list, keep_redo) {
                keep_redo = keep_redo || false;
                if(!keep_redo) {
                    this.redo_list = [];
                }

                console.log(canvas.toDataURL());
                (list || this.undo_list).push(canvas.toDataURL());
            },
            undo: function(canvas, ctx) {
                this.restoreState(canvas, ctx, this.undo_list, this.redo_list);
            },
            redo: function(canvas, ctx) {
                this.restoreState(canvas, ctx, this.redo_list, this.undo_list);
            },
            restoreState: function(canvas, ctx,  pop, push) {
                if(pop.length) {
                    this.saveState(canvas, push, true);
                    var restore_state = pop.pop();
                    var img = new Element('img', {'src':restore_state});
                    img.onload = function() {
                        ctx.clearRect(0, 0, 600, 400);
                        ctx.drawImage(img, 0, 0, 600, 400, 0, 0, 600, 400);
                    }
                }
            }
        };

        this.getTemplate = function(){
            return this.template;
        };
        this.setProduct = function(){

        };
        this.getData = function(){
            return {"id": this.id, "name": this.name, "type": this.type, "notes": this.notes};
        };
        this.removeProduct = function(){
            console.log(selectedProducts);
            selectedProducts.splice(selectedItem, 1);
            console.log(selectedProducts);
        };
        this.initCanvasObj = function(){
            if(typeof this.productFrame === 'object') return false;

            this.productFrame = new ProductFrame();
            this.productFrame.init();
        }
    }
    function ProductFrame(){
        canvas = document.getElementById('productFrame');
        ctx = canvas.getContext('2d');
        var lineWidth = rangeSlider.data('slider').getValue();
        var lineColor = colorPicker.colorpicker('getValue');

        this.isDrawing = false;

        this.init = function(e){
            this.canvas = canvas;
            this.ctx = ctx;
            this.loadBackground();
            this.addCanvasEvents();
            this.colorPickerEvents();
            this.brushSizeEvents();
        };
        this.startDraw = function(e){
            var x = e.pageX - $(canvas).offset().left;
            var y = e.pageY - $(canvas).offset().top;
            ctx.beginPath();
            artisan.drawCircle($(canvas).prop('id'), x, y, lineWidth, lineColor);
            //selectedProducts[selectedItem].history.saveState(canvas);
            this.isDrawing = true;

            console.log('action');
        };
        this.stopDraw = function(e){
            if(this.isDrawing) this.isDrawing = false;
        };
        this.draw = function(e){
            if(!this.isDrawing) return false;
            var x = e.pageX - $(canvas).offset().left;
            var y = e.pageY - $(canvas).offset().top;
            artisan.drawCircle($(canvas).prop('id'), x, y, lineWidth, lineColor);
        };
        this.brushSizeEvents = function(){
            rangeSlider.on('slide', function(e){
                lineWidth = e.value;
            });
        };
        this.colorPickerEvents = function(){
            colorPicker.on('changeColor', function(e){
                /*var color = e.color.toRGB();
                if(typeof color !== 'object') return false;
                lineColor = 'rgba('+ color.r +', '+ color.g +', '+ color.b +', '+ color.a +')';
                console.log(lineColor);*/
                lineColor = e.color.toHex();
            });
        };
        this.canvasClear = function(e){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            this.loadBackground();
        };
        this.loadBackground = function(e){
            var img = new Image();
            var preloader = document.getElementById('canvasPreloader');
            preloader.classList = 'visible';
            img.onload = function(){
                ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
                preloader.classList = '';
            };
            img.src = preview_image_url;
        };
        this.addCanvasEvents = function(){
            this.canvas.addEventListener('mousedown', this.startDraw, false);
            this.canvas.addEventListener('mouseup', this.stopDraw, false);
            this.canvas.addEventListener('mousemove', this.draw, false);
            this.canvas.addEventListener('mouseout', this.stop, false);
            document.getElementById('canvasClear').addEventListener('click', this.canvasClear, false)
        };
    }
    function FrameHistory(){
        this.redo_list = [];
        this.undo_list = [];
        this.saveState = function(canvas, list, keep_redo) {
            keep_redo = keep_redo || false;
            if(!keep_redo) {
                this.redo_list = [];
            }

            (list || this.undo_list).push(canvas.toDataURL());
        };
        this.undo = function(canvas, ctx) {
            this.restoreState(canvas, ctx, this.undo_list, this.redo_list);
        };
        this.redo = function(canvas, ctx) {
            this.restoreState(canvas, ctx, this.redo_list, this.undo_list);
        };
        this.restoreState = function(canvas, ctx,  pop, push) {
            if(pop.length) {
                this.saveState(canvas, push, true);
                var restore_state = pop.pop();
                var img = new Element('img', {'src':restore_state});
                img.onload = function() {
                    ctx.clearRect(0, 0, 600, 400);
                    ctx.drawImage(img, 0, 0, 600, 400, 0, 0, 600, 400);
                }
            }
        };
    }

});