$(function(){

    var canvasElement = $('#productFrame');
    var productsList = $('#productsList');
    //var selectedProductsList = $('#selectedProductsList');
    var products = getProducts();
    var product_types = getProductTypes();
    var selectedProducts = {};
    var counterId = 0;

    console.log(typeof selectedProducts === "object");

    if(typeof products === "object" && products.length != 0) initProductsList();

    $('.add-products-btn').on('click', function(e){
        e.preventDefault();
        selectProduct($(this));
    });

    function canvasInit(){

    }
    function initProductsList(){
        $.each(product_types, function(key, value){
            productsList.append('<label class="title">' + value.type_name + '</label>');
            getProductsByTypes(value.id);
        });
    }

    function selectProduct(elem){
        var productObj = getProductById(elem[0].dataset.productId);

        selectedProducts[counterId] = new Product(counterId, productObj.id, productObj.product, productObj.product_type);
        selectedProductsList.innerHTML += selectedProducts[counterId].getTemplate();
        counterId++;

        setActiveItem($('#selectedProductsList').find('li:last-child'));

        $('.select-product-btn').on('click', function(e){
            e.preventDefault();
            setActiveItem($(this).parent());
        });
    }

    function setActiveItem(elem){
        console.log(elem);

        var elems = $('#selectedProductsList').find('li');
        elems.removeClass('active');
        elem.addClass('active');
    }
    function getProductById(id){
        return products[id];
    }
    function getProductsByTypes(type_id){
        var listElem = document.createElement('ul');
        $.each(products, function(key, value){
            var template = '<li><a class="add-products-btn" href="#" data-product-id="' + key + '"><span>' + value.product + '</span><span class="icon icon-plus"></span></a></li>';
            if(value.product_type == type_id) listElem.innerHTML += template;
        });
        productsList.append(listElem);
    }
    function getProducts(){

        // TODO: get json data from file
        /*var result = $.getJSON(window.location.href + 'assets/products.json', function(response){
         return response.responseJSON;
         });
         console.log(result);*/

        var result = [{
            "id": "1",
            "product": "Mandelic Acid",
            "product_type": "1"
        }, {
            "id": "2",
            "product": "Botox 6ml",
            "product_type": "2"
        }, {
            "id": "3",
            "product": "Restylane 3ml",
            "product_type": "2"
        }, {
            "id": "4",
            "product": "Salcylic Acid",
            "product_type": "1"
        }, {
            "id": "5",
            "product": "Botox 2ml",
            "product_type": "2"
        }, {
            "id": "6",
            "product": "Restylane 9ml",
            "product_type": "2"
        }];

        return result || {};
    }
    function getProductTypes(){

        // TODO: get json data from file
        var result = [{
            "id": 1,
            "type_name": "Acid"
        },{
            "id": 2,
            "type_name": "Injections"
        }];

        return result || {};

    }
});
function Product(item_id, product_id, name, product_type){
    this.item_id = item_id;
    this.product_id = product_id;
    this.name = name;
    this.product_type = product_type;
    this.templateListItem = '<li><a class="select-product-btn" href="#" data-item-id="' + this.item_id + '" data-product-id="'+ this.product_id +'">'+ this.name +'</a></li>';

    this.getProductName = function(){
        return this.name;
    };
    this.getTemplate = function(){
         return this.templateListItem;
    };
}